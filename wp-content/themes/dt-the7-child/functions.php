<?php
/**
 * Your code here.
 *
 */

function wpqw_agregar_scripts() {
    wp_enqueue_style('main', get_stylesheet_directory_uri() . '/assets/css/main.css' );
    wp_enqueue_style('offside', "https://fonts.googleapis.com/css?family=Offside&display=swap" ) ;
}

add_action('wp_enqueue_scripts', 'wpqw_agregar_scripts');

function add_custom_font( $fonts ) {
$fonts['offside'] = 'offside';

return $fonts;
}
    
add_filter( 'presscore_options_get_safe_fonts', 'add_custom_font' ,30 , 1 );